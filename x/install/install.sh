#!/bin/bash
#

if [ -z $DOTFILES_DIR ]; then
    echo "DOTFILES_DIR not set.."
    exit 1
fi

BACKUP_FOLDER="$HOME/.x.bkp"
ZRESOURCES_FILE=".Xresources"

echo "Installing zsh config..."

echo "Checking if backup folder exists.."
if [ ! -d "$BACKUP_FOLDER" ]; then
    echo "Creating backup folder.."
    mkdir $BACKUP_FOLDER
else 
    echo "Backup folder exists.."
    rm -rf $BACKUP_FOLDER/*
fi


echo "Checking if .Xresources exists.."
if [ -f "$HOME/$ZRESOURCES_FILE" ]; then
    echo "Backing up .Xresources.."
    mv $HOME/$ZRESOURCES_FILE $BACKUP_FOLDER
fi

echo "Creating symlink for .Xresources.."
ln -s "$DOTFILES_DIR/x/$ZRESOURCES_FILE" "$HOME/$ZRESOURCES_FILE"
