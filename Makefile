
export DOTFILES_DIR := $(shell pwd)

DEPENDENCY_DIR := "$(DOTFILES_DIR)/lib"
DEPENDENCY_SCRIPT := "$(DEPENDENCY_DIR)/install-dep.sh"

PACKAGES := $(shell find $${DOTFILES_DIR} -mindepth 1 -maxdepth 1 -type d -not -name "lib" -not -name ".git")
PACKAGES_INSTALL_SCRIPTS := $(shell for pkg in $(PACKAGES); do echo $$pkg/install/install.sh; done)

debug:
	@echo "The workdir is: $(DOTFILES_DIR)"
	@echo "THe packages are: $(PACKAGES)"
	@echo "THe install scripts are: $(PACKAGES_INSTALL_SCRIPTS)"
	@echo "The dependency dir is: $(DEPENDENCY_DIR)"

prepare:
	@for script in $(PACKAGES_INSTALL_SCRIPTS); do \
		echo "Preparing $$script"; \
		chmod +x $$script; \
	done
install:
	@for script in $(PACKAGES_INSTALL_SCRIPTS); do \
		echo "Installing $$script"; \
		$$script || exit 123; \
	done

install-dep:
	chmod +x $(DEPENDENCY_SCRIPT)
	$(DEPENDENCY_SCRIPT)

.PHONY: debug prepare install
