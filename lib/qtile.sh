#!/bin/bash
#

if qtile -v &> /dev/null
then
    echo "qtile already installed"
    exit
fi

echo "Installing python deps"

sudo apt install -y python3 python3-pip
pip install xcffib
echo "Installing qtile"
pip install qtile

echo "Installing qtile deps"
if arandr --version &> /dev/null
then
    echo "arandr already installed"
else
    sudo apt install -y arandr
fi

if ! pavucontrol --version &> /dev/null
then
    sudo apt install -y pavucontrol
fi

if ! ranger --version &> /dev/null
then
    sudo apt install -y ranger
fi

if ! gnome-screenshot --version &> /dev/null
then
    sudo apt install -y gnome-screenshot
fi

