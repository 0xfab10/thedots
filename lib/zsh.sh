#!/bin/bash
#

echo "Installing zsh"
if ! zsh --version &> /dev/null
then
    sudo apt install -y zsh curl
fi

ZSH_BKP_FOLDER="$HOME/.zsh_backup_data"
if [ ! -d "$ZSH_BKP_FOLDER" ]; then
    echo "Creating backup folder.."
    mkdir $ZSH_BKP_FOLDER
else 
    echo "Backup folder exists.."
    rm -rf $ZSH_BKP_FOLDER/*
fi

echo "Installing oh-my-zsh"
if [ -d "$HOME/.oh-my-zsh" ]; then
    echo "Backing up oh-my-zsh.."
    mv "$HOME/.oh-my-zsh" "$ZSH_BKP_FOLDER"
fi
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

echo "Installing powerlevel10k theme"
POWERLEVEL10K_DIR="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k"
if [ -d "$POWERLEVEL10K_DIR" ]; then
    echo "Backing up powerlevel10k.."
    mv "$POWERLEVEL10K_DIR" "$ZSH_BKP_FOLDER"
fi
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $POWERLEVEL10K_DIR


echo "Installing zsh-autosuggestions"
ZSH_AUTOSUGGESTIONS_DIR="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-autosuggestions"
if [ -d "$ZSH_AUTOSUGGESTIONS_DIR" ]; then
    echo "Backing up zsh-autosuggestions.."
    mv "$ZSH_AUTOSUGGESTIONS_DIR" "$ZSH_BKP_FOLDER"
fi
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_AUTOSUGGESTIONS_DIR

echo "Installing zsh-syntax-highlighting"
ZSH_SYNTAX_HIGHLIGHTING_DIR="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting"
if [ -d "$ZSH_SYNTAX_HIGHLIGHTING_DIR" ]; then
    echo "Backing up zsh-syntax-highlighting.."
    mv "$ZSH_SYNTAX_HIGHLIGHTING_DIR" "$ZSH_BKP_FOLDER"
fi
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_SYNTAX_HIGHLIGHTING_DIR


echo "Installing zsh-vi-mode"
ZSH_VI_MODE_DIR="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-vi-mode"
if [ -d "$ZSH_VI_MODE_DIR" ]; then
    echo "Backing up zsh-vi-mode.."
    mv "$ZSH_VI_MODE_DIR" "$ZSH_BKP_FOLDER"
fi
git clone https://github.com/jeffreytse/zsh-vi-mode $ZSH_VI_MODE_DIR

export LSD_SOURCE="https://github.com/lsd-rs/lsd/releases/download/0.23.1/lsd_0.23.1_amd64.deb"
export LSD_DEB_FILE="/tmp/lsd.deb"
export LSD_INSTALL_DIR="/usr/local/bin"
if ! lsd --version &> /dev/null
then
    echo "Installing lsd"
    wget -O $LSD_DEB_FILE $LSD_SOURCE
    sudo dpkg -i $LSD_DEB_FILE
    rm $LSD_DEB_FILE
fi

if ! batcat --version &> /dev/null
then
    echo "Installing bat"
    sudo apt install -y bat
fi
