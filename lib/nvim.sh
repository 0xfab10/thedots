#!/bin/bash
#

echo "Installing Neovim"
echo "================="

echo "Installing Neovim repo"
if command -v nvim &> /dev/null
then
    echo "nvim already installed"
    exit
fi
sudo add-apt-repository -y ppa:neovim-ppa/unstable
sudo apt update

sudo apt install -y neovim
