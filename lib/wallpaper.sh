#!/bin/bash

WALLPAPER_REPO="https://github.com/D3Ext/aesthetic-wallpapers.git"
WALLPAPER_DIR="$HOME/.wallpapers"
WALLPAPER_DIR_BKP="$HOME/.wallpapers.bkp"

echo "Installing wallpapers"
if [ -d "$WALLPAPER_DIR" ]; then
    echo "Wallpaper folder exists, skipping"
    exit 0
fi

echo "Checking if backup folder exists ($WALLPAPER_DIR_BKP)"
if [ -d "$WALLPAPER_DIR_BKP" ]; then
    echo "Backup folder exists, removing it"
    rm -rf $WALLPAPER_DIR_BKP
fi

echo "Checking if wallpaper foler exists ($WALLPAPER_DIR)"
if [ -d "$WALLPAPER_DIR" ]; then
    echo "Wallpaper folder exists, backing up to $WALLPAPER_DIR_BKP"
    mv $WALLPAPER_DIR $WALLPAPER_DIR_BKP
fi


echo "Creating wallpaper folder"
mkdir -p $WALLPAPER_DIR
git clone --depth 1 $WALLPAPER_REPO /tmp/wps
mv /tmp/wps/images/* $WALLPAPER_DIR/

