#!/bin/bash
#

echo "Checking if DOTFILES_DIR is set..."
if [ -z "$DOTFILES_DIR" ]; then
    echo "DOTFILES_DIR is not set, please set it to the dotfiles directory"
    exit 1
fi


for installer in "nvim.sh" "qtile.sh" "wallpaper.sh" "zsh.sh"; do
    FILE="$DOTFILES_DIR/lib/$installer"
    echo "Installing $FILE"
    chmod +x $FILE
    $FILE || exit 1
done

if ! tmux -V &> /dev/null
then
    echo "tmux not installed"
    sudo apt install -y tmux
fi

if ! alacritty -V &> /dev/null
then
    echo "alacritty not installed"
    sudo add-apt-repository ppa:aslatter/ppa -y
    sudo apt install -y alacritty
fi

if ! fzf --version &> /dev/null
then
    echo "fzf not installed"
    sudo apt install -y fzf
fi

if ! dmenu -v &> /dev/null
then
    echo "dmenu not installed"
    sudo apt install -y dmenu
fi

if ! glow --version &> /dev/null
then
    echo "glow not installed"
    # Debian/Ubuntu
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/charm.gpg
    echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list
    sudo apt update && sudo apt install glow
fi

if [[ ! -d "$HOME/.docs/cht" ]]; then
    git clone https://gitlab.com/0xfab10/cht.git $HOME/.docs/cht
fi

if ! tldr --version &> /dev/null
then
    echo "tldr not installed"
    wget -O /tmp/tldr https://github.com/dbrgn/tealdeer/releases/download/v1.6.1/tealdeer-linux-x86_64-musl
    sudo cp /tmp/tldr /usr/local/bin/tldr
    sudo chmod +x /usr/local/bin/tldr
fi
