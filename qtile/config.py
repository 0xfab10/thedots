# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen, KeyChord, ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile.dgroups import simple_key_binder
from libqtile import hook
import os
import subprocess

import random

colors = ['#282a36', '#44475a', '#f8f8f2', '#6272a4', '#8be9fd', '#50fa7b', '#ffb86c', '#ff79c6', '#bd93f9', '#ff5555', '#f1fa8c']
backgroundColor = colors[0]
foregroundColor = colors[2]

# 
# Background	#282a36	
# Current Line	#44475a	
# Foreground	#f8f8f2	
# Comment	#6272a4	
# Cyan	#8be9fd	
# Green	#50fa7b	
# Orange	#ffb86c	
# Pink	#ff79c6	
# Purple	#bd93f9	
# Red	#ff5555	
# Yellow	#f1fa8c

WALLPAPER_DIR = "/home/fabio/.wallpapers"


def check_wallpaper_dir():
    return os.path.isdir(WALLPAPER_DIR)


def pick_random_desktop_img():
    if check_wallpaper_dir():
        wallpapers = os.listdir(WALLPAPER_DIR)
        return WALLPAPER_DIR + "/" + random.choice(wallpapers)
    else:
        return None


WALLPAPER = pick_random_desktop_img()
print(WALLPAPER)


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/auto-start.sh')
    subprocess.Popen([home])


mod = "mod4"
# terminal = guess_terminal()
terminal = "alacritty"

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod, "shift"], "d", lazy.spawn("dmenu_run"), desc="Launch terminal"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    # My keymaps
    Key([mod], "b", lazy.spawn("google-chrome"), desc="Spawn a browser"),
    Key([mod, "shift"], "b", lazy.spawn("nautilus"), desc="Spawn a browser"),

    # Screenshot
    Key([], "Print", lazy.spawn('gnome-screenshot')),
    Key([mod], "Print", lazy.spawn('gnome-screenshot -a')),

    # System
    # KeyChord([mod], "s", [
    #     Key([], "a", lazy.spawn("pavucontrol")),
    #     Key([], "d", lazy.spawn("arandr")),
    # ]),

    # Switch focus to specific monitor (out of three)
    Key([mod], "i", lazy.to_screen(0)),
    Key([mod], "o", lazy.to_screen(1)),

    # Switch focus of monitors
    Key([mod], "period", lazy.next_screen()),
    Key([mod], "comma", lazy.prev_screen()),

# Floating windows
    Key([mod, "shift"], "f",
        lazy.window.toggle_floating(),
        desc="Toggle floating",
    ),

]

groups = [
    Group("FUN", persist=True, layout="columns"),
    Group("WWW", persist=True, layout="max", spawn="google-chrome --new-window https://www.google.com/"),
    Group("DEV", persist=True, layout="max", spawn="alacritty -e tmux"),
    Group(
        "MSG",
        persist=True,
        layout="max",
        spawn="slack",
        matches=[Match(wm_class=["Slack"])]
        ),
    Group("MUS", persist=True, layout="max"),
    Group("SYS",
          persist=True,
          layout="columns",
          matches=[
                  # Match(wm_class=["arandr"]),
                  # Match(wm_class=["pavucontrol"]),
              ]
        ),
]

# allow mod3+1 through mod3+0 to bind to groups; if you bind your groups
# by hand in your config, you don't need to do this.
dgroups_key_binder = simple_key_binder(mod)

groups.append(ScratchPad("scratchpad", [
    DropDown("term", "alacritty --class=scratch", width=0.8, height=0.8, x=0.1, y=0.1, opacity=1),
    DropDown("ranger", "alacritty --class=ranger -e ranger", width=0.8, height=0.8, x=0.1, y=0.1, opacity=0.9),
    DropDown("volume", "pavucontrol", width=0.8, height=0.8, x=0.1, y=0.1, opacity=0.9),
    DropDown("display", "arandr", width=0.8, height=0.8, x=0.1, y=0.1, opacity=0.9),
    DropDown("k9s", "alacritty --class=k9s -e k9s", width=0.95, height=0.95, x=0.025, y=0.025, opacity=0.9),
]))

keys.extend([
    Key([mod], "m", lazy.group["scratchpad"].dropdown_toggle("term")),
    Key([mod], "f", lazy.group["scratchpad"].dropdown_toggle("ranger")),
    Key([mod], "v", lazy.group["scratchpad"].dropdown_toggle("volume")),
    Key([mod], "d", lazy.group["scratchpad"].dropdown_toggle("display")),
    Key([mod], "9", lazy.group["scratchpad"].dropdown_toggle("k9s")),
])


layouts = [
    layout.Columns(border_focus_stack=[foregroundColor, backgroundColor], border_width=4),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="sans",
    fontsize=14,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        wallpaper=WALLPAPER,
        wallpaper_mode='fill',
        top=bar.Bar(
            [
                widget.CurrentLayout(
                    foreground=foregroundColor,
                    background=backgroundColor,
                ),
                widget.GroupBox(
                    font="Hack Nerd Font",
                    fontsize = 16,
                    margin_y = 2,
                    margin_x = 4,
                    padding_y = 6,
                    padding_x = 6,
                    borderwidth = 2,
                    disable_drag = True,
                    active = colors[4],
                    inactive = foregroundColor,
                    hide_unused = True,
                    rounded = False,
                    highlight_method = "line",
                    highlight_color = [backgroundColor, backgroundColor],
                    this_current_screen_border = colors[5],
                    this_screen_border = colors[7],
                    other_screen_border = colors[6],
                    other_current_screen_border = colors[6],
                    urgent_alert_method = "line",
                    urgent_border = colors[9],
                    urgent_text = colors[1],
                    foreground = foregroundColor,
                    background = backgroundColor,
                    use_mouse_wheel = False

                ),
                widget.Prompt(fontsize=14, prompt="Run: "),
                widget.WindowName(
                    fontsize=14,
                    foreground=foregroundColor,
                    background=backgroundColor,
                ),
                widget.Chord(
                    chords_colors={
                        "launch": (backgroundColor, foregroundColor),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Sep(),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                # widget.CPUGraph(),
                widget.PulseVolume(
                    fontsize=14,
                    foreground=foregroundColor,
                    background=backgroundColor,
                ),
                widget.KeyboardLayout(fontsize=14),
                widget.Systray(
                    background=backgroundColor,
                    icon_size=20,
                    foreground=foregroundColor,
                    padding=4,
                ),
                widget.Clock(format="%Y-%m-%d %a %I:%M %p", fontsize=14),
                # widget.QuickExit(),
            ],
            24,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
    Screen(
        wallpaper=WALLPAPER,
        wallpaper_mode='fill',
    )
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

# dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        # Match(wm_class="arandr"),  # arandr
        # Match(wm_class="pavucontrol"),  # pavucontrol
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
