#!/bin/bash

QTILE_CONFIG_FILE="$HOME/.config/qtile/config.py"
QTILE_CONFIG_FOLDER="$HOME/.config/qtile"
QTILE_CONFIG_FOLDER_BKP="$HOME/.config/qtile.bkp"
XSESSION_DIR="/usr/share/xsessions"
XSESSION_FILE="qtile.desktop"

if [ -z "$DOTFILES_DIR" ]; then
    echo "DOTFILES_DIR is not set, please set it to the dotfiles directory"
    exit 1
fi

echo "Installing qtile config..."

echo "Checking if $QTILE_CONFIG_FOLDER_BKP already exists..."
if [ -f "$QTILE_CONFIG_FOLDER_BKP" ]; then
    echo "Backup folder $QTILE_CONFIG_FOLDER_BKP already exists, removing it..."
    rm -rf $QTILE_CONFIG_FOLDER_BKP
fi

echo "Checking if $QTILE_CONFIG_FOLDER already exists..."
if [ -d "$QTILE_CONFIG_FOLDER" ]; then
    echo "Folder already exists, backing up to $QTILE_CONFIG_FOLDER_BKP..."
    mv $QTILE_CONFIG_FOLDER/* $QTILE_CONFIG_FOLDER_BKP
fi

echo "Creating $QTILE_CONFIG_FOLDER..."
mkdir -p $QTILE_CONFIG_FOLDER

ln -s "$DOTFILES_DIR/qtile/config.py" "$QTILE_CONFIG_FILE"
chmod +x "$DOTFILES_DIR/qtile/auto-start.sh"
ln -s "$DOTFILES_DIR/qtile/auto-start.sh" "$QTILE_CONFIG_FOLDER/auto-start.sh"

echo "Checking if old xsession file exists..."
if [ -f "$XSESSION_DIR/$XSESSION_FILE" ]; then
    echo "Old xsession file exists, backing it up..."
    mv "$XSESSION_DIR/$XSESSION_FILE" "$XSESSION_DIR/$XSESSION_FILE.bkp"
fi

echo "Creating xsession file..."
sudo cp "$DOTFILES_DIR/qtile/$XSESSION_FILE" "$XSESSION_DIR/$XSESSION_FILE"
