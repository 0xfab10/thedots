#!/bin/bash

ALACRITTY_CONFIG_FILE=~/.config/alacritty/alacritty.yml
ALACRITTY_CONFIG_FOLDER=~/.config/alacritty
ALACRITTY_CONFIG_FOLDER_BKP=~/.config/alacritty.bkp

if [ -z "$DOTFILES_DIR" ]; then
    echo "DOTFILES_DIR is not set, please set it to the dotfiles directory"
    exit 1
fi

echo "Installing alacritty config..."

echo "Checking if $ALACRITTY_CONFIG_FOLDER_BKP already exists..."
if [ -d "$ALACRITTY_CONFIG_FOLDER_BKP" ]; then
    echo "Backup folder $ALACRITTY_CONFIG_FOLDER_BKP already exists, removing it..."
    rm -rf $ALACRITTY_CONFIG_FOLDER_BKP/*
else
    mkdir -p $ALACRITTY_CONFIG_FOLDER_BKP
fi

echo "Checking if $ALACRITTY_CONFIG_FOLDER already exists..."
if [ -d "$ALACRITTY_CONFIG_FOLDER" ]; then
    echo "Folder already exists, backing up to $ALACRITTY_CONFIG_FOLDER_BKP..."
    mv $ALACRITTY_CONFIG_FOLDER/* $ALACRITTY_CONFIG_FOLDER_BKP
fi

echo "Creating $ALACRITTY_CONFIG_FOLDER..."
mkdir -p $ALACRITTY_CONFIG_FOLDER

ln -s "$DOTFILES_DIR/alacritty/alacritty.yml" "$ALACRITTY_CONFIG_FILE"
ln -s "$DOTFILES_DIR/alacritty/tokyo-night.yml" "$ALACRITTY_CONFIG_FOLDER/tokyo-night.yml"
