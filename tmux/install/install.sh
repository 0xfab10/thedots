#!/bin/bash

TMUX_CONFIG_FILE=~/.tmux.conf
TMUX_CONFIG_FILE_BKP=~/.tmux.conf.bkp

if [ -z "$DOTFILES_DIR" ]; then
    echo "DOTFILES_DIR is not set, please set it to the dotfiles directory"
    exit 1
fi

echo "Installing tmux config..."

echo "Checking if ~/.tmux.conf already exists..."
if [ -f "$TMUX_CONFIG_FILE" ]; then
    echo "File already exists, backing up to $TMUX_CONFIG_FILE_BKP..."
    mv $TMUX_CONFIG_FILE $TMUX_CONFIG_FILE_BKP
fi

ln -s $DOTFILES_DIR/tmux/tmux.conf $TMUX_CONFIG_FILE
