#!/bin/bash

NVIM_CONFIG_DIR=~/.config/nvim
NVIM_CONFIG_DIR_BKP=~/.config/nvim.bkp
NVIM_CONFIG_REPO=git@gitlab.com:0xfab10/nvim-config.git
echo "Installing neovim config..."

echo "Checking if backup folder exists and deleting it..."
if [ -d "$NVIM_CONFIG_DIR_BKP" ]; then
    rm -rf "$NVIM_CONFIG_DIR_BKP"
fi

echo "Checking if folder already exists..."
if [ -d "$NVIM_CONFIG_DIR" ]; then
    echo "Folder already exists, backing it up to ${NVIM_CONFIG_DIR_BKP}..."
    mv "$NVIM_CONFIG_DIR" "$NVIM_CONFIG_DIR_BKP"
fi

git clone $NVIM_CONFIG_REPO $NVIM_CONFIG_DIR
cd $NVIM_CONFIG_DIR
git checkout 0xfab10
cd -

if ! rg --version &> /dev/null; then
    echo "Installing ripgrep..."
    sudo apt-get install -y ripgrep
fi
