#!/bin/bash
#

if [ -z $DOTFILES_DIR ]; then
    echo "DOTFILES_DIR not set.."
    exit 1
fi

BACKUP_FOLDER="$HOME/.zsh_backup"
ZSHRC=".zshrc"
ZSH_ALIASES=".zsh_aliases"
PROFILE=".profile"
BASHRC=".bashrc"
BASH_ALIASES=".bash_aliases"
BASH_PROFILE=".bash_profile"
P10K_CONFIG=".p10k.zsh" 

echo "Installing zsh config..."

echo "Checking if backup folder exists.."
if [ ! -d "$BACKUP_FOLDER" ]; then
    echo "Creating backup folder.."
    mkdir $BACKUP_FOLDER
else 
    echo "Backup folder exists.."
    rm -rf $BACKUP_FOLDER/*
fi


for file in $ZSHRC $PROFILE $BASHRC $BASH_ALIASES $BASH_PROFILE $P10K_CONFIG; do
    echo "Working on $file.."
    if [ -f "$HOME/$file" ]; then
        echo "Backing up $file ..."
        mv "$HOME/$file" "$BACKUP_FOLDER"
    fi
    if [ -L "$HOME/$file" ]; then
        echo "Removing existing symlink $file.."
        rm "$HOME/$file"
    fi
    echo "Linking $file.."
    ln -s "$DOTFILES_DIR/zsh/$file" "$HOME/$file"
done

echo "Linking .zsh_aliases.."
if [ -f "$HOME/$ZSH_ALIASES" ]; then
    echo "Backing up $ZSH_ALIASES ..."
    mv "$HOME/$ZSH_ALIASES" "$BACKUP_FOLDER"
fi
if [ -L "$HOME/$ZSH_ALIASES" ]; then
    echo "Removing existing symlink $ZSH_ALIASES.."
    rm "$HOME/$ZSH_ALIASES"
fi
ln -s "$DOTFILES_DIR/zsh/$BASH_ALIASES" "$HOME/$ZSH_ALIASES"
