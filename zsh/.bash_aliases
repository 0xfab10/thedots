
# aliases
alias cd..='cd ..'
alias cp='cp -iv'
alias chmod="chmod -c"
alias df='df -h -x squashfs -x tmpfs -x devtmpfs'

# Use lsd instead of ls
alias ls='lsd -lhF --color=auto'
alias l='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'

# Use bat instead of cat
alias cat='batcat'
alias fzf_prev='fzf --preview "batcat --color=always --style=header,grid --line-range :500 {}"'

alias mkdir='mkdir -pv'
alias ports='netstat -tulanp'
alias h='history'
alias mv='mv -iv'
alias svim='sudo vim'
alias wget='wget -c'

## get top process eating memory
alias mem5='ps auxf | sort -nr -k 4 | head -5'
alias mem10='ps auxf | sort -nr -k 4 | head -10'

## get top process eating cpu ##
alias cpu5='ps auxf | sort -nr -k 3 | head -5'
alias cpu10='ps auxf | sort -nr -k 3 | head -10'

## List largest directories (aka "ducks")
alias dir5='du -cksh * | sort -hr | head -n 5'
alias dir10='du -cksh * | sort -hr | head -n 10'

# Safetynets
# do not delete / or prompt if deleting more than 3 files at a time #
alias rm='rm -I --preserve-root'

# confirmation #
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'

# Parenting changing perms on / #
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

# reload bash config
alias reload="source ~/.bashrc"

alias kctl=kubectl

alias k=kubectl

alias kctx=kubectx

alias kns=kubens

# Weather
alias wttr='curl "https://wttr.in/Berlin?Fn0"';
alias wttr-detailed='curl "https://wttr.in/Berlin?F&format=v2d"';


