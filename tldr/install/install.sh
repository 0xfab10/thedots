#!/bin/bash

TEALDEER_CONFIG_FILE=$HOME/.config/tealdeer/config.toml
TEALDEER_CONFIG_FOLDER=$HOME/.config/tealdeer/
TEALDEER_CONFIG_FOLDER_BKP=$HOME/.config/tealdeer.bkp

if [ -z "$DOTFILES_DIR" ]; then
    echo "DOTFILES_DIR is not set, please set it to the dotfiles directory"
    exit 1
fi

echo "Installing  config..."

echo "Checking if $TEALDEER_CONFIG_FOLDER_BKP already exists..."
if [ -d "$TEALDEER_CONFIG_FOLDER_BKP" ]; then
    echo "Backup folder $TEALDEER_CONFIG_FOLDER_BKP already exists, removing it..."
    rm -rf $TEALDEER_CONFIG_FOLDER_BKP/*
else
    mkdir -p $TEALDEER_CONFIG_FOLDER_BKP
fi

echo "Checking if $TEALDEER_CONFIG_FOLDER already exists..."
if [ -d "$TEALDEER_CONFIG_FOLDER" ]; then
    echo "Folder already exists, backing up to $TEALDEER_CONFIG_FOLDER_BKP..."
    mv $TEALDEER_CONFIG_FOLDER/* $TEALDEER_CONFIG_FOLDER_BKP
fi

echo "Creating $TEALDEER_CONFIG_FOLDER..."
mkdir -p $TEALDEER_CONFIG_FOLDER

ln -s "$DOTFILES_DIR/tldr/config.toml" "$TEALDEER_CONFIG_FILE"
