#!/bin/bash

PACKAGE_NAME="scripts"

if [ -z "$DOTFILES_DIR" ]; then
    echo "DOTFILES_DIR not set, exiting..."
    exit 1
fi

SCRIPT_FOLDER=~/.scripts
SCRIPT_FOLDER_BKP=~/.scripts.bkp
FULL_PATH="$DOTFILES_DIR/$PACKAGE_NAME"

SCRIPTS=$(find "$FULL_PATH" -maxdepth 1 -type f -not -name "install.sh" -not -name "uninstall.sh" -not -name "README.md" -not -name "LICENSE")


echo "Installing scripts..."


echo "Checking if backup folder exists at $SCRIPT_FOLDER_BKP"
if [ -d "$SCRIPT_FOLDER_BKP" ]; then
    echo "Backup folder exists, removing..."
    rm -rf $SCRIPT_FOLDER_BKP
fi

echo "Checking if scripts folder exists at $SCRIPT_FOLDER"
if [ -d "$SCRIPT_FOLDER" ]; then
    echo "Scripts folder exists, moving to backup folder $SCRIPT_FOLDER_BKP..."
    mv $SCRIPT_FOLDER $SCRIPT_FOLDER_BKP
fi


echo "Creating scripts folder..."
mkdir -p $SCRIPT_FOLDER

for SCRIPT in $SCRIPTS; do
    echo "Installing $SCRIPT..."
    chmod +x $SCRIPT
    ln -s "$SCRIPT" $SCRIPT_FOLDER
done

