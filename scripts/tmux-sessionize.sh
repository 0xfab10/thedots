#!/usr/bin/env bash

get_my_dirs() {
    find ~/Code -mindepth 2 -maxdepth 2 -type d
    find ~/.config -mindepth 1 -maxdepth 1 -type d
    find ~/Documents -mindepth 1 -maxdepth 1 -type d
}

if [[ $# -eq 1 ]]; then
    selected=$1
else
    selected=$(get_my_dirs | fzf)
fi

if [[ -z $selected ]]; then
    exit 0
fi

selected_name=$(basename "$selected" | tr . _)
tmux_running=$(pgrep tmux)

if [[ -z $TMUX ]] && [[ -z $tmux_running ]]; then
    tmux new-session -s $selected_name -c $selected
    exit 0
fi

if ! tmux has-session -t=$selected_name 2> /dev/null; then
    tmux new-session -ds $selected_name -c $selected
fi

tmux switch-client -t $selected_name
