#!/bin/bash
#
HELP_DIR=$HOME/.docs/cht

selected="$1"
if [[ -z $selected ]]; then
    selected=$(find $HELP_DIR -type f -name "*.md" | fzf --print-query --prompt="Pick a language: " | tail -1)
fi
 
if [[ -z $selected ]]; then
    exit 0
fi

glow $selected -p
