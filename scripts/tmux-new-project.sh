#!/bin/bash
#
CODE_DIR=$HOME/Code
namespace=$(find $CODE_DIR -mindepth 1 -maxdepth 1 -type d -exec basename {} \;| fzf --print-query --prompt="Pick a namespace: " | tail -1)
echo $namespace
if [[ -z $namespace ]]; then
    exit 0
fi
parent_dir=$CODE_DIR/$namespace
if [[ ! -d $parent_dir ]]; then
    mkdir -p $parent_dir
fi

PROJECT_NEW="New Project"
PROJECT_CLONE="Clone Project"
clone_or_new=$(echo -e "$PROJECT_NEW\n$PROJECT_CLONE" | fzf --print-query --prompt="Clone or new?: " | tail -1)

# echo $clone_or_new
export CLONE_OR_NEW=$clone_or_new
if [[ -z $clone_or_new ]]; then
    echo "No clone or new"
    exit 0
fi
project_name=""
project_dir=""
echo "Clone or new: $clone_or_new"
if [[ $clone_or_new == $PROJECT_NEW ]]; then
    echo "Is new proj"
    project_name=$(echo "" | fzf --print-query --prompt="Project name: " | tail -1 | tr . _)
    echo "Project name: $project_name"
    if [[ -z $project_name ]]; then
        exit 0
    fi
    project_dir="$parent_dir/$project_name"
    echo "Project dir: $project_dir"

    if [[ -d $project_dir ]]; then
        echo "Project dir already exists"
        exit 1
    fi
    mkdir -p $project_dir
    
elif [[ $clone_or_new == $PROJECT_CLONE ]]; then
    echo "Is clone proj"
    project_url=$(echo "" | fzf --print-query --prompt="Project url: " | tail -1)
    echo "Project url: $project_url"
    if [[ -z $project_url ]]; then
        exit 0
    fi

    project_name=$(basename $project_url '.git' | tr . _)
    echo "Project name: $project_name"
    project_dir="$parent_dir/$project_name"
    echo "Project dir: $project_dir"

    if [[ -d $project_dir ]]; then
        echo "Project dir already exists"
        exit 1
    fi
    git clone $project_url $project_dir
    if [[ $? -ne 0 ]]; then
        echo "Git clone failed"
        exit 1
    fi
    if [[ ! -d $project_dir ]]; then
        echo "Project dir does not exist"
        exit 1
    fi
fi

if [[ -z $project_name ]] || [[ -z $project_dir ]]; then
    echo "No project name or dir"
    exit 0
fi

echo "Project name: $project_name"
echo "Project dir: $project_dir"


# echo "Project name: $project_name"
# echo "Project dir: $project_dir"
#

tmux_running=$(pgrep tmux)
if [[ -z $TMUX ]] && [[ -z $tmux_running ]]; then
    echo "No tmux session running"
    tmux new-session -s $project_name -c $project_dir
    exit 0
fi

if ! tmux has-session -t=$project_name 2> /dev/null; then
    echo "No tmux session for project"
    tmux new-session -ds $project_name -c $project_dir
fi

echo "Switching to tmux session"
tmux switch-client -t $project_name
